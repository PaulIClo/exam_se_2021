import javax.swing.*;

public class exam2 extends JFrame {

    public exam2() {
        setTitle("Exam");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);

        JTextField field = new JTextField();
        field.setBounds(75, 20, 150, 40);

        JTextArea area = new JTextArea();
        area.setBounds(75, 100, 150, 40);

        JButton button = new JButton();
        button.setBounds(200, 200, 60, 40);


        add(field);
        add(area);
        add(button);

        button.addActionListener(e -> area.setText(area.getText() + field.getText()));

    }

    public static void main(String[] args) {
        new exam2();
    }

}
