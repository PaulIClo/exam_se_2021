import java.util.ArrayList;
import java.util.List;

class B {
    private long t;
    private List<D> list;
    private E field;
    public void f() {

    }
    public void metJ(A j) {

    }

    public void addD(D elemD){
        list.add(elemD);
    }
}
class A {

}
class D {

}
class E extends C {
    private List<F> field = new ArrayList<>();
    public E() {
    }
    public void addF() {
        field.add(new F());
    }
    public void metG(int i) {

    }
}
class F {
    public void metA() {

    }
}
class C {

}